module trig_mode(input trig_en,auto_norm,clk,output reg rst);
	reg[9:0] count=0;
	always@(posedge clk)begin
		if(auto_norm==1)
			rst=trig_en;
		else if(count==1000 && trig_en==0)begin
			rst=1;
			count=0;
		end
		else begin
			rst=trig_en;
		end
		if(count>=1000)
			count=0;
		count=count+1;
	end
endmodule
