module rgbSel(input[9:0] x,y,input r1,g1,b1,r2,g2,b2,output ro,go,bo);
	assign ro=(x>=299 && x<339 && y>=400 && y<436)?r2:r1;
	assign go=(x>=299 && x<339 && y>=400 && y<436)?g2:g1;
	assign bo=(x>=299 && x<339 && y>=400 && y<436)?b2:b1;
endmodule