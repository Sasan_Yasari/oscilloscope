module clkDvdr(input clk,output clk_out);
	reg [1:0] count=0;
	always@(posedge clk)begin
		count=count+1;
	end
	assign clk_out = count[0];
endmodule