module trigger(input slope,clk,input[7:0] level,sample,output reg trig_en);
	reg[7:0] old_sample;
	always@(posedge clk)begin
		if(slope==1'b0 && level<=sample && level>=old_sample)
			trig_en=1;
		else if(slope==1'b1 && level>=sample && level<=old_sample)
			trig_en=1;
		else trig_en=0;
		old_sample=sample;
	end
endmodule