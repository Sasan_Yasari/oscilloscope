module triangle(input clk, output reg[7:0] out = 0);
	reg[7:0] counter = 0;
	reg flag = 0;
	always@(posedge clk) begin
		counter = counter + 1;
		if(counter == 1) begin
			counter = 0;
			if(flag == 0)
				out = out + 2;
			else
				out = out - 2;
			if(out == 254 || out == 0)
				flag = ~flag;
		end
	end
endmodule