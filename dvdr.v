module Dvdr(input clk,output reg clk_out);
	reg [15:0] count=0;
	always@(posedge clk)begin
		count=count+1;
		if(count<3125)
			clk_out=1'b1;
		else if(count<6250)
			clk_out=1'b0;
		else if(count==6250)
			count=0;
	end
endmodule
