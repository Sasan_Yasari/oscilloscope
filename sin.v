module sin(input clk, reset, output reg[7:0] out = 0);
	reg[18:0] counter = 0;
	reg signed[15:0] sin, sin_old, cos, cos_old;
	always@(posedge clk) begin
		counter = counter + 1;
		if(reset) begin
			sin = 0;
			cos = 30000;
		end
		if(counter == 2) begin
			counter = 0;
			sin_old = sin;
			cos_old = cos;
			sin = sin_old + {cos_old[15],cos_old[15],cos_old[15],cos_old[15],cos_old[15],cos_old[15],cos_old[15:6]};
			cos = cos_old - {sin[15],sin[15],sin[15],sin[15],sin[15],sin[15],sin[15:6]};
		end
		out = sin[15:8] + 128;
	end
endmodule