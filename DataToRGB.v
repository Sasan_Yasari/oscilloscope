module DataToRGB(input[7:0] data,input[9:0] x,output r,g,output reg b);
	assign r=0;
	assign g=0;
	always@(x)begin
			if((x==299 || x==307 || x==315 || x==323 || x==331 )&& data[7]==1)b=1;
			else if((x==300 || x==308 || x==316 || x==324 || x==332)  && data[6]==1)b=1;
			else if((x==301 || x==309 || x==317 || x==325 || x==333) && data[5]==1)b=1;
			else if((x==302 || x==310 || x==318 || x==326 || x==334) && data[4]==1)b=1;
			else if((x==303 || x==311 || x==319 || x==327 || x==335) && data[3]==1)b=1;
			else if((x==304 || x==312 || x==320 || x==328 || x==336) && data[2]==1)b=1;
			else if((x==305 || x==313 || x==321 || x==329 || x==337) && data[1]==1)b=1;
			else if((x==306 || x==314 || x==322 || x==330 || x==338) && data[0]==1)b=1;
			else b=0;
			/*
			if(x>=283 && x<363)begin
			case(xx[3:1])
				0: r = ~data[7];
				1: r = ~data[6];
				2: r = ~data[5];
				3: r = ~data[4];
				4: r = ~data[3];
				5: r = ~data[2];
				6: r = ~data[1];
				7: r = ~data[0];
			endcase
			*/
		end
	end
endmodule