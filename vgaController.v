module vgaController(input r,g,b,clk,output ro,go,bo,output h_sync,v_sync,output[9:0] x,y);
	reg[9:0] x_count=0;
	reg[9:0] y_count=0;
	always@(posedge clk)begin
		x_count<=x_count+1; 
		if(x_count==699)
			y_count<=y_count+1;
		if(y_count>524)
			y_count<=0;
		if(x_count>799)
			x_count<=0;
	end
	assign ro=(x_count%30==0)?1:(y_count%30==0)?1:(x_count>639 || y_count>479)?0:r;
	assign go=(x_count>639 || y_count>479)?0:g;
	assign bo=(x_count>639 || y_count>479)?0:b;
	assign h_sync=(x_count>658 && x_count<756)?0:1;
	assign v_sync=(y_count>493 && y_count<496)?0:1;
	assign x=x_count;
	assign y=y_count;
endmodule
