module cmp(input[7:0] in1,input[9:0] in2,output eq);
	assign eq=({2'b00,in1}==in2[9:0])?1:0;
endmodule