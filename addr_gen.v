module addr_gen(input clk,input rst,output reg[9:0] addr);
	reg[9:0] count=0;
	always@(posedge clk)begin
		if(count==640 && rst==1)
			count=0;
		else if(count!=640)
			count=count+1;
		addr=count;
	end
endmodule