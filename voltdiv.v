module voltdiv(input[7:0] in,input[1:0] sel,output reg[7:0] out);
	always@(sel)begin
		case(sel)
			0:out=in;
			1:out=in>>1;
			2:out=in>>2;
			3:out=in>>3;
		endcase
	end
endmodule
