module time_div(input clk,input[1:0] sel,output reg clk_out);
	reg[15:0] count=0;
	always@(posedge clk)begin
		if(sel==0)begin
			if(count<25000)
				clk_out=1'b1;
			else if(count<50000)
				clk_out=1'b0;
			else if(count==50000)
				count=0;
		end
		else if(sel==1)begin
			if(count<12500)
				clk_out=1'b1;
			else if(count<25000)
				clk_out=1'b0;
			else if(count==25000)
				count=0;
		end
		else if(sel==2)begin
			if(count<6250)
				clk_out=1'b1;
			else if(count<12500)
				clk_out=1'b0;
			else if(count==12500)
				count=0;
		end
		else if(sel==3)begin
			if(count<3125)
				clk_out=1'b1;
			else if(count<6250)
				clk_out=1'b0;
			else if(count==6250)
				count=0;
		end
		count=count+1;
	end
endmodule