module DFG(input[2:0] func, input reset, clk, output reg[7:0] out);
	wire[7:0] out1, out2, out3, out4, out5;
	square SQ(clk, out1);
	saw SA(clk, out2);
	triangle TR(clk, out3);
	sin SI(clk, reset, out4);
	rhomboid RH(clk, out5);
	always@(posedge clk) begin
		if(func == 3'b000)
			out = out5;
		else if(func == 3'b001)
			out = out4;
		else if(func == 3'b010)
			out = out1;
		else if(func == 3'b011)
			out = out3;
		else if(func == 3'b100)
			out = out2;
	end
endmodule