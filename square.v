module square(input clk, output reg[7:0] out = 0);
	reg[7:0] counter = 0;
	always@(posedge clk) begin
		counter = counter + 1;
		if(counter == 127)
			out = ~out;
	end
endmodule