module rhomboid(input clk, output reg[7:0] out = 0);
	reg[7:0] counter = 0;
	reg flag = 0;
	reg[7:0] count = 0;
	reg[7:0] out1, out2;
	always@(posedge clk) begin
		counter = counter + 1;
		if(counter < 128) begin
			if(counter % 2)
				out = 128 + counter;
			else
				out = 128 - counter;
		end
		else begin
			if(counter % 2)
				out = 128 + 256 - counter;
			else
				out = 128 - 256 + counter;
		end
	end
endmodule